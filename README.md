# ComMan UI

This project provide an frontend for the application [ComMan](https://gitlab.com/HeavenSleep/comman)

## Usage

### Docker

```bash
docker run -d -p 8080:8080 registry.gitlab.com/heavensleep/comman-ui:master
```

### Configuration

Here are all availables environment variable

| Variables     | Value                                       | Description                      |
| ------------- | ------------------------------------------- | -------------------------------- |
| API_HOST      | `string` (default: `http://127.0.0.1:8000`) | URL to join the ComMan API       |
| API_BASE_PATH | `string` (default: `/api/v1`)               | Base path to join the ComMan API |

## Development

### Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install

# bun
bun install
```

### Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev

# bun
bun run dev
```

### Production

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build

# bun
bun run build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview

# bun
bun run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
