# Build stage for building the app
FROM node:16-alpine as builder
WORKDIR /app
COPY ./src .
RUN npm install
RUN npx nuxi generate

# Final stage with Nginx
FROM nginx:alpine
WORKDIR /usr/share/nginx/html

# Remove default nginx static assets
RUN rm -rf ./*

# Copy static assets from builder stage
COPY --from=builder /app/.output/public .

# Adjust Nginx to listen on a non-root port (e.g., 8080)
RUN sed -i 's/listen       80;/listen 8080;/' /etc/nginx/conf.d/default.conf
EXPOSE 8080

# Add a new user 'nginxuser' and switch to it
RUN adduser -D -H -u 1001 -s /bin/sh -g "Nginx User" -G root nginxuser

# Create required directories and adjust permissions
RUN mkdir -p /var/cache/nginx/client_temp \
    && mkdir -p /var/cache/nginx/proxy_temp \
    && mkdir -p /var/cache/nginx/fastcgi_temp \
    && mkdir -p /var/cache/nginx/uwsgi_temp \
    && mkdir -p /var/cache/nginx/scgi_temp \
    && chown -R nginxuser /var/cache/nginx \
    && chmod -R 775 /var/cache/nginx \
    && chmod 775 /var/run

# Switch to non-root user
USER 1001

# Start Nginx and keep the process running
CMD ["nginx", "-g", "daemon off;"]
