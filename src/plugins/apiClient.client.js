import axios from 'axios';
import { defineNuxtPlugin } from '#app';
import { useAuthStore } from '~/stores/auth';

export default defineNuxtPlugin((nuxtApp) => {
  const store = useAuthStore();

  const apiHost = nuxtApp.$config.public.apiHost; // Either from env or default
  const apiBasePath = nuxtApp.$config.public.apiBasePath; // Either from env or default

  const apiClient = axios.create({
    baseURL: `${apiHost}${apiBasePath}`,
    headers: {
      'Content-Type': 'application/json'
    }
  });

  apiClient.interceptors.request.use(config => {
    const token = store.accessToken;
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  });

  apiClient.interceptors.response.use(
    response => response,
    async (error) => {
      const originalRequest = error.config;
      if (error.response.status === 401 && !originalRequest._retry && store.refreshToken) {
        originalRequest._retry = true;
        try {
          await store.refreshAccessToken();
          apiClient.defaults.headers.common['Authorization'] = 'Bearer ' + store.accessToken;
          return apiClient(originalRequest);
        } catch (refreshError) {
          store.logout();
          return Promise.reject(refreshError);
        }
      } else if (error.response.status > 400 && error.response.status < 500) {
        store.logout();
      }
      return Promise.reject(error);
    }
  );

  nuxtApp.provide('apiClient', apiClient);
});
