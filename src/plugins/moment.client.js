import moment from "moment";

export default defineNuxtPlugin((nuxtApp) => {
    return {
        provide: {
            relative:(d) => moment(d).fromNow(),
            formatDate:(d, f) => moment(d).format(f)
        }
    }
})