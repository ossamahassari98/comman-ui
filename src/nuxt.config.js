// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  devtools: {
    enabled: process.env.DEBUG || false,
  },
  modules: ['@pinia/nuxt', '@nuxt/ui'],
  alias: {
    pinia: "/node_modules/@pinia/nuxt/node_modules/pinia/dist/pinia.mjs"
  },
  css: ['@/assets/css/main.css'],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  colorMode: {
    classSuffix: '',
    preference: 'system',
    fallback: 'dark'
  },

  runtimeConfig: {
    public: {
      apiHost: process.env.API_HOST || 'http://127.0.0.1:8000',
      apiBasePath: process.env.API_BASE_PATH || '/api/v1'
    }
  }
})
