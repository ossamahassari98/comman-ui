// stores/auth.js
import { defineStore } from 'pinia';
import { useNuxtApp, useRouter } from '#app';

export const useAuthStore = defineStore('auth', {
  state: () => ({
    username: null,
    accessToken: null,
    refreshToken: null,
    remember: false
  }),

  actions: {
    async login(credentials) {
      try {
        const { $apiClient } = useNuxtApp();
        const response = await $apiClient.post('/token/', credentials);

        this.accessToken = response.data.access;
        this.refreshToken = response.data.refresh;
        this.username = credentials.username;
        this.remember = credentials.remember;

        if (this.remember) {
          localStorage.setItem('accessToken', this.accessToken);
          localStorage.setItem('refreshToken', this.refreshToken);
          localStorage.setItem('username', this.username);
          localStorage.setItem('remember', this.remember);
        }
      } catch (error) {
        throw error;
      }
    },

    logout() {
      this.username = null;
      this.accessToken = null;
      this.refreshToken = null;
      this.remember = false;

      localStorage.removeItem('accessToken');
      localStorage.removeItem('refreshToken');
      localStorage.removeItem('username');
      localStorage.removeItem('remember');

      // redirect to login page
      const router = useRouter();
      router.push('/login');
    },

    isTokenExpired(token) {
      const base64Url = token.split(".")[1];
      const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
      const jsonPayload = decodeURIComponent(
        atob(base64)
          .split("")
          .map(function (c) {
            return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
          })
          .join("")
      );
    
      const { exp } = JSON.parse(jsonPayload);
      const expired = Date.now() >= exp * 1000
      return expired
    },

    initAuth() {
      const accessToken = localStorage.getItem('accessToken');
      const refreshToken = localStorage.getItem('refreshToken');
      const remember = localStorage.getItem('remember');
      const username = localStorage.getItem('username');

      if (accessToken && refreshToken) {
        if (this.isTokenExpired(refreshToken)) {
          return false;
        }
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
      }

      if (username) {
        this.username = username;
      }

      if (remember) {
        this.remember = remember;
      }
    },

    async refreshAccessToken() {
      try {
        const { $apiClient } = useNuxtApp();

        if (this.isTokenExpired(this.refreshToken)) {
          throw new Error('Refresh token is expired');
        }

        const response = await $apiClient.post('/token/refresh/', {
          refresh: this.refreshToken
        });

        this.accessToken = response.data.access;

        if (this.remember) {
          localStorage.setItem('accessToken', this.accessToken);
        }
      } catch (error) {
        this.logout(); // If token refresh fails, log out the user
        throw new Error('Token refresh failed');
      }
    }
  }
});
